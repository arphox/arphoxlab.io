---
title: Process Explorer
date: 2020-11-16
tags: ["tools"]
---

[**Process Explorer**](https://docs.microsoft.com/en-us/sysinternals/downloads/process-explorer) is a free tool created by Mark Russinovich, and is available on docs.microsoft.com, meaning it is probably a trustworthy program. It is a portable program, no install required.

The above link has a nice introduction about what it does, but let me give you some ideas. 
It lists all processes on your machine, and for each process, you can see:
- process name, CPU and memory consumption, PID, etc.
- path where the process is running from, and the command line
- its "autostart location" registry key
- all threads owned by the process and some information about them
- managed TCP/IP ports
- environment variables
- and much more.

One use case the program is particularly useful is to **find which program has a particular file or directory open** (so if you don't know why you can't delete a file it may give you the answer).

You can also use the "Lower Pane" (has to be turned on at View menu) to see all handles or DLLs a process has/uses.